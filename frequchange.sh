#!/bin/bash
# Copyright 2020 Fe-Ti <btm.007@mailru>
clear
echo "Запуск скрипта для ручного изменения частоты CPU"

echo "Проверка наличия прав администратора..."
status="researching"
if dmesg > /dev/null
then
echo "OK"
status='READY'
# вывод если не от администратора
else
echo "Нет прав."
fi

#Основной блок скрипта
#if [ $status = "READY" ]
#then
echo "
-+-<Скрипт изменения частоты CPU версия 3.0>-+-
"

#инициализация параметров
action="change"
cpus=`grep processor /proc/cpuinfo | wc -l`

#Главный цикл
while ([ $action != "q" ])
do

if [ $action = "s" ]
then
clear
echo "s - вывод этой справки
q - выход
a - изменение частоты всех потоков (ядер)
e - изменение частоты каждого потока (ядер) отдельно
l - установка в автозагрузку пресета частот
p - пресеты частот
v - задать конкретную частоту
ON - включение сервиса в systemd
OFF - отключение сервиса в systemd
license - договор с конечным пользователем"


#Изменение для всех ядер
elif [ $action = "a" ]
then
clear
#########################################################################################
echo "Введите минимальную частоту в МГц (только цифры)"
minfreq=0
read minfreq
minfreq=$(( $minfreq * 1000 ))
echo "Введите максимальную частоту в МГц (только цифры)"
maxfreq=0
read maxfreq
maxfreq=$(( $maxfreq * 1000 ))
echo "Введите политику управления частотой, допустимые значения:"
echo `cat /sys/devices/system/cpu/cpufreq/policy0/scaling_available_governors`
governor=""
while ( [[ "`grep -ow "${governor}" /sys/devices/system/cpu/cpufreq/policy0/scaling_available_governors`" = "" ]] )
do
read governor
done

for (( core=0 ; core<$cpus ; core++))
do
echo "Изменение частот ядра ${core}..."
echo $governor > /sys/devices/system/cpu/cpufreq/policy${core}/scaling_governor
echo $maxfreq > /sys/devices/system/cpu/cpufreq/policy${core}/scaling_max_freq
echo $minfreq > /sys/devices/system/cpu/cpufreq/policy${core}/scaling_min_freq
done
#########################################################################################


#изменение для каждого отдельно
elif [ $action = "e" ]
then

#########################################################################################
for (( core=0 ; core<$cpus ; core++ ))
do
clear
echo "Введите минимальную частоту ядра №${core} в МГц (только цифры)"
minfreq=0
read minfreq
minfreq=$(( $minfreq * 1000 ))
echo "Введите максимальную частоту ядра №${core} в МГц (только цифры)"
maxfreq=0
read maxfreq
maxfreq=$(( $maxfreq * 1000 ))

echo "Введите политику управления частотой, допустимые значения:"
echo `cat /sys/devices/system/cpu/cpufreq/policy0/scaling_available_governors`
governor=""
while ( [[ "`grep -ow "${governor}" /sys/devices/system/cpu/cpufreq/policy0/scaling_available_governors`" = "" ]] )
do
read governor
done

echo "Изменение частот ядра ${core}..."
echo $governor > /sys/devices/system/cpu/cpufreq/policy${core}/scaling_governor
echo $maxfreq > /sys/devices/system/cpu/cpufreq/policy${core}/scaling_max_freq
echo $minfreq > /sys/devices/system/cpu/cpufreq/policy${core}/scaling_min_freq

done
#########################################################################################

#установка частоты при загрузке системы
elif [ $action = "l" ]
then
clear
#########################################################################################

echo "Введите минимальную частоту в МГц (только цифры)"
minfreq=0 
read minfreq
minfreq=$(( $minfreq * 1000 ))
echo "Введите максимальную частоту в МГц (только цифры)"
maxfreq=0
read maxfreq
maxfreq=$(( $maxfreq * 1000 ))
echo "Введите политику управления частотой, допустимые значения:"
echo `cat /sys/devices/system/cpu/cpufreq/policy0/scaling_available_governors`
governor=""
while ( [[ "`grep -ow "${governor}" /sys/devices/system/cpu/cpufreq/policy0/scaling_available_governors`" = "" ]] )
do
read governor
done

echo 'Создаю скрипт autofrequ...'

if [[ ! -e /etc/init.d ]]
then
mkdir /etc/init.d
fi

cd /etc/init.d

echo '#!/bin/bash
#
### BEGIN INIT INFO
# Provides:       autofrequ
# Required-Start: $remote_fs $syslog dbus
# Required-Stop:  $remote_fs $syslog dbus
# Default-Start:  2 3 4 5
# Default-Stop:   0 1 6
# Short-Description: Setting frequency into a range
### END INIT INFO

cpus=`grep processor /proc/cpuinfo | wc -l`

for (( core=0 ; core<$cpus ; core++ ))
do

#"Изменение частот ядра ${core}..."
echo '${governor}' > /sys/devices/system/cpu/cpufreq/policy${core}/scaling_governor
echo '${maxfreq}' > /sys/devices/system/cpu/cpufreq/policy${core}/scaling_max_freq
echo '${minfreq}' > /sys/devices/system/cpu/cpufreq/policy${core}/scaling_min_freq
done
' > autofrequ

chmod +x autofrequ
pathto=`pwd` #путь до скрипта с автозагрузкой параметров
echo 'Autofrequ находится в каталоге '${pathto}

echo 'Добавляю скрипт autofrequ в автозагрузку...'

if systemctl --version > /dev/null
then

cd /etc/systemd/system
touch autofrequ.service
echo "[Unit]
Description=Script_For_Setting_Custom_Frequency
After=syslog.target

[Service]
ExecStart=/bin/bash '${pathto}/autofrequ'
Type=forking
OOMScoreAdjust=-100
RestartSec=10
Restart=on-fail

[Install]
WantedBy="`systemctl get-default`"
Alias=bash.service" > autofrequ.service
echo "systemctl daemon-reload"
systemctl daemon-reload
echo "systemctl disable autofrequ"
systemctl disable autofrequ
echo "systemctl enable autofrequ"
systemctl enable autofrequ
echo "systemctl start autofrequ.service"
systemctl start autofrequ.service
cd -
pwd
cd ..

elif ls /etc/rc3.d > /dev/null
then
cd /etc/init.d
rm autofrequ.sh
chmod +x autofrequ
cd /etc/rc5.d
[[ `rm -v autofrequ` ]]
ln -v ${pathto}/autofrequ autofrequ

else

echo "Не найден sysvinit|openrc|systemd. Автозагрузка невозможнa."

fi
#########################################################################################
#фиксация определенной частоты
#########################################################################################
elif [ $action = 'v' ]
then
echo "Введите частоту от `cat /sys/devices/system/cpu/cpufreq/policy0/cpuinfo_min_freq` до `cat /sys/devices/system/cpu/cpufreq/policy0/cpuinfo_max_freq`"
read freq
freq=$(( $freq * 1000 ))
for (( core=0 ; core<$cpus ; core++))
do
echo "Изменение частот ядра ${core}..."
echo performance > /sys/devices/system/cpu/cpufreq/policy${core}/scaling_governor
echo $freq > /sys/devices/system/cpu/cpufreq/policy${core}/scaling_max_freq
echo $freq > /sys/devices/system/cpu/cpufreq/policy${core}/scaling_min_freq
done
#########################################################################################
#включение и отключение сервиса в автозагрузке
#########################################################################################
elif [ $action = 'ON' ]
then
echo "systemctl enable autofrequ"
systemctl enable autofrequ
echo "systemctl start autofrequ.service"
systemctl start autofrequ.service
elif [ $action = 'OFF' ] 
then
echo "systemctl disable autofrequ"
systemctl disable autofrequ

elif [ $action = 'license' ]
then
clear
echo "Лицензия frequchange:
1. Используя скрипт frequchange.sh (далее - скрипт) Вы соглашаетесь с лицензией frequchange.

2. Скрипт предоставляется 'как есть', т.е. автор не гарантирует его работоспособности,
   а также не несет ответственности за возможный ущерб, причинённый выполнением скрипта.

3. Автор разрешает свободное распространение скрипта,
   а также полное или частичное его копирование в другие проекты.

4. Изменение скрипта распространение его под тем же названием без согласия автора запрещено.

Примечание:
Подразумевается, что, запуская этот скрипт, пользователь ознакомлен с правилами работы от имени учетной записи администратора."

fi

#предложение для ввода команды

echo "
Введите команду 
(s - просмотреть список команд; q - выйти)"
read action

#конец цикла
done

#else
#echo "Возникла какая-то ошибка."
#fi

#clear
