# Frequchange

**RU version**

Интерактивный скрипт для Linux позволяющий контроллировать частоту CPU.

#### Применение:

1. Запустите скрипт с правами root;
2. Следуйте инструкции из справки.

#### Возможности:

* Установка пользовательских частот и политики их изменения для всех ядер(в пределах возможностей CPU и матплаты).

**EN version (WIP)**

Interactive Linux shell script for controlling CPU frequency.

Usage:

Just run `./frequchange.sh` with root privilegies and then follow the instructions